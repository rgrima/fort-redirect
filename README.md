# fort-redirect

Redirection in GNU fortran compiler is different from INTEL.

## GFORTRAN

```bash
$ gfortran --version
GNU Fortran (GCC) 11.3.1 20221121 (Red Hat 11.3.1-4)

$ make FC=gfortran
gfortran -o hello.gfortran hello.F90

$ ./hello.gfortran
 Hello world *
 Hello world 6

$ cat gfortran.out 
 Redirected(gfortran): Hello world *
 Redirected(gfortran): Hello world 6
```



## IFORT

```bash
$ ifort --version
ifort (IFORT) 2021.10.0 20230609
Copyright (C) 1985-2023 Intel Corporation.  All rights reserved.

$ make FC=ifort
ifort -o hello.ifort hello.F90

$ ./hello.ifort 
 Hello world *
 Hello world 6
 Redirected(ifort): Hello world *

$ cat ifort.out 
 Redirected(ifort): Hello world 6
```




## IFX

```bash
$ ifx --version
ifx (IFX) 2023.2.0 20230622
Copyright (C) 1985-2023 Intel Corporation. All rights reserved.

$ make FC=ifx
ifx -o hello.ifx hello.F90

$ ./hello.ifx
 Hello world *
 Hello world 6
 Redirected(ifx): Hello world *

$ cat ifx.out
 Redirected(ifx): Hello world 6
```



