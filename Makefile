FC=gfortran

hello.$(FC): hello.F90
	$(FC) -o $@ $<

clean:
	rm -rf hello.gfortran hello.ifort hello.ifx gfortran.out  ifort.out ifx.out
