#!/usr/bin/env python

from os import listdir, system
from os.path import join, isfile, isdir
import sys
import tty
import termios

from termios import tcgetattr, tcsetattr, TCSANOW
from sys import stdin
from tty import setraw
verbose=False
PROCESS_EXT=[ "F", "F90", "T90", "f90", "f", "inc", "lit" ]

IGNORE_EXT=[ "c", "txt", "sh", "make", "md", "py", "README", "files", "LICENSE",
             "routines", "project", "bash", "in", "org", "build", "gitmodules",
             "gitignore", "NEWS", "am", "INSTALL", "AUTHORS", "COPYING",
             "ChangeLog", "ac", "cpp", "psml", "mk", "dat" ]
IGNORE_EXT=[ "c", "cpp", "txt", "sh", "make", "md", "py", "files", "routines",
             "bash", "in", "org", "build", "ac", "psml", "mk", "dat", "sed",
             "mf", "html", "m4" ]

DIRS=[ "Src" ]
files=[]

def getchar():
    fd = stdin.fileno()
    attr = tcgetattr(fd)
    try:
        setraw(fd)
        return stdin.read(1)
    finally:
        tcsetattr(fd, TCSANOW, attr)

ALWAYS=False
def get_Y_or_N( msg ) :
    global ALWAYS
    print(msg, end='',flush=True)
    while True :
        c = getchar()
        if c.lower() == "y" :
            print( c )
            return True
        elif c.lower() == "n" :
            print( c )
            return False
        elif c.lower() == "a" :
            print( c )
            ALWAYS=True
            return True
        elif c.lower() == "q" :
            print( c )
            exit("Quitting")

def processTag( line ) :
    i=0
    c=line[i]
    TAGS=[ c ]
    i += 1
    while len(TAGS)>0 and i<len(line) :
        c=line[i]
        i += 1
        if TAGS[-1]=="'" and c!="'" : continue
        if TAGS[-1]=="\"" and c!="\"" : continue
        if c.isalnum() or c in [" ","=",">","<",".","_","*","%",":",",","/","+","\n","-","&"] : continue
        if c==")" :
            if TAGS[-1]=="(" :
                TAGS.pop(); continue
            print( "TAGS:", TAGS )
            print( "   c:", c )
            raise TypeError("Error at processTag")
        elif c=="(" :
            if TAGS[-1]=="(" :
                TAGS+=[ c ]; continue
            print( "TAGS:", TAGS )
            print( "   c:", c )
            raise TypeError("Error at processTag")
        elif c in [ "'", "\"" ] :
            if TAGS[-1]==c : TAGS.pop()
            else : TAGS+=[ c ]
        else :
            print( line, end='' )
            print( "   Process c ["+c+"]", ord(c) )
            raise TypeError("Error at processTag")
    return line[:i], line[i:]

def processConditional( line ) :
    lilo=line.lower()
    i=0
    if lilo.startswith( "if" ) :
        i=2
        while line[i] in [ " ", "\t" ] : i+=1
    elif lilo.startswith( "elseif" ) :
        i=6
        while line[i] in [ " ", "\t" ] : i+=1
    elif lilo.startswith( "else if" ) :
        i=7
        while line[i] in [ " ", "\t" ] : i+=1
    else :
        print( "prCond  line:", line, end='' )
        print( "processConditional not if" )
        raise TypeError("Error at processConditional")
    c=line[i]
    if c != "(" :
        print( "prCond  line:", line, end='' )
        print( "processConditional not '('" )
        raise TypeError("Error at processConditional")
    try:
        tag,desc = processTag(line[i:])
    except:
        print( "prCond  line:", line, end='' )
        raise TypeError("Error at processConditional")
    return desc.lstrip()

def splitline( line ) :
    if len(line)>73 :
        print( len(line), line, end='' )
        if get_Y_or_N("Abort [y/n]") :
            raise ValueError("Wrong data")
    return line


def processPrint( line, mod, fixedf ) :
    li = line.lstrip()
    lilo = li.lower()
    if lilo.startswith( "print(...)" ) : return mod, line
    if lilo.startswith( "if" ) or lilo.startswith( "else" ) :
        try :
            li = processConditional( li )
        except:
            exit("Error at processWrite")
        lilo = li.lower()
    if not lilo.startswith( "print" ) : return mod, line
    sec=li[5:]
    if sec[0]==" " : sec = sec.lstrip()
    c=sec[0]
    if c=="*" :
        mod = True
        if "," not in sec : sec=sec[:-1]+",\n"
        npre=len(line)-len(li)
        pos=sec.split(",",1)[1]
        npos=len(pos)
        if fixedf and npre+10+npos>73 :
            line = line[:npre]+"write(6,*)"+((63-npre)*" ")+"&\n"+ \
                   (5*" ")+"."+((npre-6)*" ")+line[-npos:]
        else :
            line = line[:npre]+"write(6,*)"+line[-npos:]
    elif c in [ "'", "\"", "(" ] :
        mod = True
        try:
            tag,desc = processTag( sec )
        except:
            print( "processPrint  line:", line, end='' )
            exit("Error at processPrint")
        desc = desc.split(",",1)[1] if "," in desc else desc
        line = line[:len(line)-len(li)]+"write(6,"+tag+")"+desc
        if (fixedf) : line = splitline( line )
    return mod, line


def processWrite( line, mod, fixedf ) :
    li = line.lstrip()
    lilo = li.lower()
    if lilo.startswith( "if" ) or lilo.startswith( "else" ) :
        try :
            li = processConditional( li )
        except:
            exit("Error at processWrite")
        lilo = li.lower()
    if not lilo.startswith( "write" ) : return mod, line
    sec=li[5:]
    if sec[0]==" " : sec = sec.lstrip()
    if sec[0] != "(" : return mod, line
    i=len(line)-len(sec)+1
    try :
        tag, desc = processTag( sec )
    except :
        print( "processWrite line=", line, end='' )
        exit(0)
    tt = tag[1:-1].split(",",1)
    if tt[0]=="*" :
        mod = True
        tag=tag.replace("*","6",1)
        dsc = desc.split("!")[0].strip()
        if len(dsc)==0 and len(tt)>1 and tt[1]=="*" :
            tag=tag.replace("*","'(A)'",1)
        line = line[:-len(sec)]+tag+desc
        if (fixedf) : line = splitline( line )
    return mod, line

def processFile( fn ) :
    fext=fn.rsplit(".",1)[-1]
    fixedf = fext in [ "f", "F" ]
    mod=False
    fd=open("rgt.out","w")
    for line in open(fn).readlines() :
        li=line.lstrip().split("!")[0].lower()
        try :
            if "print" in li :
                mod, line = processPrint( line, mod, fixedf )
            elif "write" in li :
                mod, line = processWrite( line, mod, fixedf )
        except :
            print( "processFile", fn )
            exit(0)
        fd.write( line )
    fd.close()
    if (False) : return
    if (mod) :
        print( "diff "+fn+" rgt.out" )
        system( "diff "+fn+" rgt.out" )
        if ALWAYS or get_Y_or_N( "Overwrite file "+fn+" [(y)es/(n)o/(a)lways/(q)uit]: " ) :
            print("cp rgt.out "+fn)
            system( "cp rgt.out "+fn )

if False :
    #verbose=True
    f="Src/m_broyden_mixing.f"
    processFile( f )
    exit(0)
while len(DIRS)>0 :
    DIR=DIRS.pop(0)
    for f in listdir(DIR) :
        fi = join(DIR,f)
        if isfile(fi) :
            if f.startswith( "Makefile." ) or \
               f.startswith( "make." ) or \
               f.startswith( ".git" ) or \
               len(f.rsplit(".",1))==1 :
                continue
            fext=f.rsplit(".",1)[-1]
            if fext in IGNORE_EXT : continue
            if fext not in PROCESS_EXT :
                print( "Check ", fi, "fext:", fext )
                exit(0)
            processFile( fi )
        else : DIRS+=[ fi ]
    


